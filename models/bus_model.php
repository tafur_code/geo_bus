<?php
class bus_model
{
  private $db;
  private $bus;
  private $parkingCount;

  public function __construct()
  {
    $this->db = Conectar::conexion();
    $this->admins = array();
    $this->parkingCount =  new parking_model();
  }

  public function insertBus(
    $bus_driver,
    $bus_origin,
    $bus_destination,
    $bus_stay_time,
    $bus_departure_time_point_origin,
    $bus_arrival_time_point_destination,
    $bus_passengers,
    $parking_id
  ) {

    if ($this->parkingCount->updateParking($parking_id) != NULL) {

      $stmt = $this->db->query(
        "INSERT INTO `tbl_bus`(`idtbl_bus`, `bus_driver`, `bus_origin`, `bus_destination`, `bus_stay_time`, `bus_departure_time_point_origin`, `bus_arrival_time_point_destination`, `bus_passengers`, `tbl_parking_idtbl_parking`)
      VALUES (
        null,
        '" . $bus_driver . "',
        '" . $bus_origin . "',
        '" . $bus_destination . "',
        " . $bus_stay_time . ",
        '" . $bus_departure_time_point_origin . "',
        '" . $bus_arrival_time_point_destination . "',
        " . $bus_passengers . ","
          . $parking_id .
          ")"
      );

      if ($stmt) {
        $this->parkingCount->updateParking($parking_id);
        echo " <div id='alert' class='alert'> <div class='alert__box'> <h1>Datos guardados correctamente</h1> </div> </div>";
      } else {
        echo "<div id='alert' class='alert'> <h1>No se han guardado los datos</h1> </div>";
      }
    }
  }
}
