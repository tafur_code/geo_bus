<?php

class parking_model
{
  private $db;
  private $parkings;
  private $parking;
  private $bus;
  private $count;
  private $capacity;

  public function __construct()
  {
    $this->db = Conectar::conexion();
    $this->parkings = array();
    $this->parking = array();
    $this->bus = array();
    $this->count = 0;
    $this->capacity = 0;
  }

  public function list_parkings()
  {
    $query = $this->db->query("SELECT * FROM tbl_parking;");
    while ($row = $query->fetch_assoc()) {
      $this->parkings[] = $row;
    }

    return $this->parkings;
  }

  public function detail_parkin($id)
  {
    $query = $this->db->query("SELECT * FROM tbl_parking WHERE idtbl_parking = " . $id . ";");
    $two_query = $this->db->query("SELECT * FROM tbl_bus JOIN tbl_parking ON tbl_bus.tbl_parking_idtbl_parking = tbl_parking.idtbl_parking WHERE tbl_parking_idtbl_parking = " . $id . ";");

    if ($query) {
      while ($row = $query->fetch_assoc()) {
        $data = [
          'nombre' => $row['parking_municipality'],
          'Localizacion' => $row['parking_coordinates'],
          'Cupos' => $row['parking_bus_capacity'],
          'cupos_disponibles' =>  $row['parking_total_buses'],
          'schedule_conductor' => $row['parking_schedule_conductors']
        ];
      }

      while ($row = $two_query->fetch_assoc()) {
        $this->bus[] = $row;
      }

      array_push($this->parking, $data);

      return [$this->parking, $this->bus];
    }
  }

  public function countCapacityParking($id_parking)
  {

    $query = $this->db->query("SELECT COUNT(tbl_bus.idtbl_bus) as count, tbl_parking.parking_bus_capacity as parking_capactiy
    FROM tbl_bus
    JOIN tbl_parking ON
    tbl_bus.tbl_parking_idtbl_parking = tbl_parking.idtbl_parking
    WHERE tbl_parking.idtbl_parking =" . $id_parking . ";");


    while ($row = $query->fetch_assoc()) {
      $this->count = $row['count'];
      $this->capacity = $row['parking_capactiy'];
    }

    return [$this->count, $this->capacity];
  }

  public function updateParking($id_parking)
  {
    $dataUpdate = false;

    if ($this->countCapacityParking($id_parking)[1] == $this->countCapacityParking($id_parking)[0]) {
      echo "<div id='alert-error' class='alert'> <div class='alert__box'> <h1>ESTE APARCADERO YA TIENE SU CUPO LLENO</h1> </div></div>";
      $dataUpdate = false;
      $query = $this->db->query("UPDATE tbl_parking
      SET parking_total_buses = 0 WHERE idtbl_parking = " . $id_parking . "");
    } else {

      $restaCupos = $this->capacity - $this->count;

      $query = $this->db->query("UPDATE tbl_parking
      SET parking_current_buses = " . $this->count . ", parking_total_buses = " . $restaCupos . "
      WHERE idtbl_parking = " . $id_parking . "");

      $dataUpdate = true;
    }
    return $dataUpdate;
  }
}
