$(document).ready(function () {
  setInterval(() => {
    if ($("#alert").is(":visible")) {
      setTimeout(() => {
        $("#alert").css("display", "none");
      }, 2000);
    }
    if ($("#alert-error").is(":visible")) {
      setTimeout(() => {
        $("#alert-error").css("display", "none");
      }, 2000);
    }
  }, 1000);
});
