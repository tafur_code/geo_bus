const arrJson = document.querySelector("code").innerHTML;
var myJson = JSON.parse(arrJson);

var mymap = L.map("mapa").setView([5.0198, -74.0781], 9);

L.tileLayer(
  "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw",
  {
    minZoom: 11,
    attribution:
      'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
      'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: "mapbox/streets-v11",
    tileSize: 512,
    zoomOffset: -1,
  }
).addTo(mymap);

var geoJSONLeafler = [];

myJson.forEach(function (item) {
  const lat_lon = item.parking_coordinates.split(", ");

  const dataGeoJSON = {
    type: "Feature",
    properties: {
      id: item.idtbl_parking,
      name: item.parking_municipality,
      locate: item.parking_coordinates,
      capacity: item.parking_bus_capacity,
    },
    geometry: {
      type: "Point",
      coordinates: [lat_lon[1], lat_lon[0]],
    },
  };

  geoJSONLeafler.unshift(dataGeoJSON);
});

function onMapClick(e) {
  if (e.feature) {
    const inputParking = document.querySelector("#id_parking");
    inputParking.setAttribute("value", e.feature.properties.id);
  }
}

L.geoJSON(geoJSONLeafler)
  .bindPopup(function (layer) {
    onMapClick(layer);
    return layer.feature.properties.name;
  })
  .addTo(mymap);

mymap.on("click", onMapClick);
