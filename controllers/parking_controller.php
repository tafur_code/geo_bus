<?php
//Llamada al modelo
require_once("models/parking_model.php");

class ParkingController
{
  var $parkings = "";
  var $data = "";
  var $bus = "";

  function __construct()
  {
    $this->parkings = new parking_model();
    $this->data = $this->parkings->list_parkings();
  }

  public function map_of_parkings()
  {
    $data_table = $this->data;
    $json_parking = json_encode($data_table);

    require_once("views/parking_view.phtml");
  }
}
